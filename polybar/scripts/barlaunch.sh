#!/usr/bin/env bash

# Terminate already running bar instances
# If all your bars have ipc enabled, you can use
polybar-msg cmd quit
# Otherwise you can use the nuclear option:
# killall -q polybar

polybar e-DP1 &

external_monitors=$(xrandr --query | grep 'DP1-')
if [[ $external_monitors = *connected* ]]; then
    polybar middle &
    polybar right &
fi
