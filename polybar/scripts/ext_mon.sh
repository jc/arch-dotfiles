#!/bin/bash





external_monitors=$(xrandr --query | grep 'DP1-')
if [[ $external_monitors = *connected* ]]; then
    xrandr --output DP1-3 --mode 1024x768 --rotate normal --right-of eDP1 --output DP1-1 --mode 2560x1440 --rotate normal --right-of DP1-3

fi
